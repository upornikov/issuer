from django import forms
from app.models import currency_choices, transaction_choices, transfer_choices
from app.models import Transaction


class TransactionForm(forms.ModelForm):
    type = forms.ChoiceField(help_text='Transaction Type', choices=transaction_choices[1:3])
    card_id = forms.CharField(max_length=32, help_text='Card ID')
    transaction_id = forms.CharField(help_text='Transaction ID', max_length=32)
    billing_currency = forms.ChoiceField(help_text='Billing currency', choices=currency_choices)
    billing_amount = forms.DecimalField(help_text='Billing amount', max_digits=10, decimal_places=2)
    settlement_currency = forms.ChoiceField(help_text='Settlement currency', choices=currency_choices)
    settlement_amount = forms.DecimalField(help_text='Settlement amount', max_digits=10, decimal_places=2, required=False)
    class Meta:
        model = Transaction
        fields = ('type', 'card_id', 'transaction_id', 
                  'billing_currency', 'billing_amount', 
                  'settlement_currency', 'settlement_amount',
                 )
	