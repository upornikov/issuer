from django.contrib import admin

from app.models import Account, Transaction, Transfer

admin.site.register(Account)
admin.site.register(Transaction)
admin.site.register(Transfer)
