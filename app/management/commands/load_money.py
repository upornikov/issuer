from django.core.management.base import BaseCommand
from app.models import Account
from app.models import currency_choices

class Command(BaseCommand):
    help = 'Add money to the card account, correspondingly increase balance on the wallet.\n' \
           'If does not exist, create the account.'

    c_ch = [c for c, _ in currency_choices]
    
    def add_arguments(self, parser):
        parser.add_argument('card_id', nargs='+', type=str)
        parser.add_argument('amount', nargs='+', type=float)
        parser.add_argument('currency', nargs='+', type=str, help=str(self.c_ch))

    def handle(self, *args, **options):
        currency = options['currency'][0].upper()
        try:
            if currency not in self.c_ch:
                raise Exception('Unsupported currency {}'.format(options['currency'][0]))
                
            account, created = Account.objects.get_or_create(
                        card_id=options['card_id'][0], currency=currency)
            data = dict(
                    type='refill',
                    card_id=options['card_id'][0],
                    # transaction_id is optional; random string will be generated, if none
                    refill_currency=currency,
                    refill_amount=str(round(options['amount'][0], 2)),
                    )
            account.do_refill(data)
        except Exception as e:
            self.stdout.write('load_money: {}'.format(str(e)))
            exit(1)
        
        if created:
            msg = 'Account {0} was created and its balance was updated: {1} {2}'. \
                        format(account.card_id, account.amount, account.currency)
        else:
            msg = 'Account {0} balance was updated: {1} {2}'.format(account.card_id, account.amount, account.currency)
        self.stdout.write(msg)
        exit(0)
                
