from django.core.management.base import BaseCommand
from django.db.models import Sum
from app.models import Transaction
from app.models import do_bulk_settlement

class Command(BaseCommand):
    help = 'Do clearing on presented transactions, get resulting balance.'
            
    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        try:
            qs = Transaction.objects.filter(type='presentment')
            revenue = qs.aggregate(rev=Sum('fee'))['rev'] or 0
            payment = qs.aggregate(pay=Sum('settlement_amount'))['pay'] or 0
            do_bulk_settlement()
        except Exception as e:
            self.stdout.write('do_settlement: {}'.format(str(e)))
            exit(1)
            
        self.stdout.write('Clearing success. Payment/Revenue: {0}/{1}'.format(payment, revenue))
        exit(0)
                
