from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.db.models import Sum
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db.models.signals import pre_save
from django.dispatch import receiver
from decimal import Decimal
import json
import random
import string


def random_string(n):
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(n))

currency_choices = (
    ('EUR', 'EUR'),
    ('USD', 'USD'),
    ('FR', 'FR'),
)

transaction_choices = (
    ('refill', 'refill'),
    ('authorisation', 'authorisation'),
    ('presentment', 'presentment'),
    ('settlement', 'settlement'),
)

transfer_choices = (
    ('debit', 'debit'),
    ('credit', 'credit'),
)

transaction_fields = [
    'type', 'card_id', 'transaction_id', 
    'merchant_name', 'merchant_city', 'merchant_country', '"merchant_mcc',
    'billing_amount', 'billing_currency', 
    'transaction_amount', 'transaction_currency', 
    'settlement_amount', 'settlement_currency',
    'refill_amount', 'refill_currency',
]    


def validate_transaction_data(data):
    # validate json schema, if needed
    # fraud/anomaly detection, etc.
    pass
    
def Validate(condition, message):
    if not condition:
        raise ValidationError(message)

def do_bulk_settlement():
    """ Do clearing and transfer in bulk for all currencies. """
    transfers = []
    for currency, _ in currency_choices:
        transfers.append(do_settlement(currency))
    return transfers          
    
def do_settlement(currency):
    """ Do bulk clearing and transfer for transactions in the currency. """
    transactions = Transaction.objects.filter(type='presentment', currency=currency)
    if not transactions:
        return None
    transfer = Transfer.objects.create(
                type='debit',
                currency=currency,
                )
    transfer.transaction_set.set(transactions)
    transfer.run()
    transactions.update(type='settlement', settlement_date=timezone.now())           
    return transfer


          
class Account(models.Model):
    # card_id uniquely identifies the Account
    card_id = models.CharField(max_length=128, unique=True, verbose_name='Card ID')
    currency = models.CharField(
            max_length=8, choices=currency_choices, default='EUR', verbose_name='Currency type')
    amount = models.DecimalField(
            max_digits=10, decimal_places=2, default=Decimal('0.00'), 
            verbose_name='Debit amount on the account')
    
    def __str__(self):
        return '{0} {1} {2}'.format(self.card_id, self.currency, self.amount)
        
    def validate_transaction_data(self, data):
        validate_transaction_data(data)
        Validate(data.get('card_id') == self.card_id, 'Invalid card ID: %s vs %s'%(data.get('card_id'), self.card_id))
        
    def do_authorisation(self, data):
        """ Handler for webhook authorization transaction """
        def validate_input():
            self.validate_transaction_data(data)
            Validate(data.get('type') == 'authorisation', 'Invalid transaction type')
            Validate(data.get('transaction_id'), 'Missing transaction ID')
            Validate(data.get('billing_currency') == self.currency, 'Unsupported currency')
            Validate(not self.transaction_set.filter(transaction_id=data['transaction_id']), 'Transaction already exists')
        
        validate_input()
        Transaction.create_authorisation_transaction(self, data)
        
    def do_presentment(self, data):
        """ Handler for webhook presentment transaction """
        def validate_input():
            self.validate_transaction_data(data)
            Validate(data.get('type') == 'presentment', 'Invalid transaction type')
            Validate(data.get('transaction_id'), 'Missing transaction ID')
            Validate(data.get('billing_currency') == self.currency, 'Unsupported or missing currency')
            Validate(self.amount >= Decimal(data.get('billing_amount', 0)), 'Not enough funds')
            Validate(data.get('settlement_currency') == self.currency, 'Unsupported or missing currency')
            Validate(self.amount >= Decimal(data.get('settlement_amount', 0)), 'Not enough funds for settlement')
        
        validate_input()
        transaction = self.transaction_set.get(transaction_id=data['transaction_id'])
        transaction.do_presentment(data)
        self.amount -= Decimal(data['billing_amount'])
        self.save()

    # do_settlement(self, data)...
    # No settlement on account level - this is a bulk operation on transactions, 
    # and by settlement time account is already in consistent state  
           
    def do_refill(self, data):
        """ Add balance to the account. """
        def validate_input():
            self.validate_transaction_data(data)
            Validate(data.get('type') == 'refill', 'Invalid transaction type')
            Validate(data.get('refill_currency') == self.currency, 'Unsupported currency')
        
        validate_input()
        transfer = Transfer.objects.create(
                    type='credit',
                    currency=data['refill_currency'],
                    amount=data['refill_amount']
                    )
        ledger_amount_after_transaction = self.amount + Decimal(data['refill_amount'])
        available_amount_after_transaction = ledger_amount_after_transaction - self.get_reserved_amount()
        transaction_id=data.get('transaction_id') or random_string(8)
        Validate(not Transaction.objects.filter(transaction_id=transaction_id),  \
              'do_refill: Invalid transaction_id {}'.format(transaction_id))
        transaction = Transaction.objects.create(
                        type=data['type'],
                        date=timezone.now(),
                        transaction_id=data.get('transaction_id') or random_string(8),
                        currency=data['refill_currency'],
                        refill_amount=data['refill_amount'],
                        transfer=transfer,
                        ledger_amount=ledger_amount_after_transaction,
                        available_amount=available_amount_after_transaction,
                        transaction_data = json.dumps( {data['type']: data} )
                        )
        transfer.run()
        self.transaction_set.add(transaction)
        self.amount = ledger_amount_after_transaction
        self.save()
    
    def get_reserved_amount(self):
        return self.transaction_set.filter(type='authorisation').aggregate(reserved=Sum('billing_amount'))['reserved'] or 0
        
    def get_transactions(self, start_date=None, end_date=None):
        """
        Get all transactions, excluding authorisation type, for a specific timeframe. 
        If no dates, return all the transactions from the account.
        If start_date is None, return transactions before the end_date.
        If end_date is None, return transactions since the start_date.
        
        start_date, end_date -- datetime (django.utils.timezone) objects for the timeframe
        """
        if start_date and end_date:
            ts = self.transaction_set.filter(date__range=(start_date, end_date))
        elif start_date:
            ts = self.transaction_set.filter(date__gte=start_date)
        elif end_date:
            ts = self.transaction_set.filter(date__lte=end_date)
        else:
            ts = self.transaction_set
        ts = ts.filter(type='refill') | ts.exclude(presentment_date__isnull=True)
        return ts
        
    def get_presented_transactions(self, start_date=None, end_date=None):
        """
        Get presented transactions for a specific timeframe (including settled ones) 
        If no dates, return all the transactions from the account.
        If start_date is None, return transactions before the end_date.
        If end_date is None, return transactions since the start_date.
        
        start_date, end_date -- datetime (django.utils.timezone) objects for the timeframe
        """
        if start_date and end_date:
            ts = self.transaction_set.filter(date__range=(start_date, end_date))
        elif start_date:
            ts = self.transaction_set.filter(date__gte=start_date)
        elif end_date:
            ts = self.transaction_set.filter(date__lte=end_date)
        else:
            ts = self.transaction_set
        ts = ts.exclude(type='refill').exclude(presentment_date__isnull=True)
        return ts
        

    def get_ledger_amount(self, date=None):
        """
        Get ledger amount on the card for the date. 
        If date is None, return current ledger amount.
        
        date -- datetime (django.utils.timezone) object
        """
        try:
            if not date:
                transaction = self.transaction_set.latest('date')
            else:
                transaction = self.transaction_set.filter(date__lte=date).last()
        except ObjectDoesNotExist:
            return dict(balance=Decimal('0.0'))
        return dict(amount=transaction.ledger_amount)
        
    def get_available_amount(self, date=None):
        """
        Get available amount on the card for the date (authorised 
        reservations accounted for). If date is None, return available amount.
        
        date -- datetime (django.utils.timezone) object
        """
        try:
            if not date:
                transaction = self.transaction_set.latest('date')
            else:
                transaction = self.transaction_set.filter(date__lte=date).last()
        except ObjectDoesNotExist:
            return dict(balance=Decimal('0.0'))
        return dict(amount=transaction.available_amount)


    def get_ledger_balance(self, date=None):
        """
        Get a balance of presented transactions on the date. 
        If date is None, return current ledger balance.
        
        date -- datetime (django.utils.timezone) object
        """        
        try:
            if date:
                transactions = self.transaction_set.filter(presentment_date__isnull=False, presentment_date__lte=date). \
                                                    exclude(settlement_date__isnull=False, settlement_date__lte=date)
            else:
                transactions = self.transaction_set.exclude(presentment_date__isnull=True).exclude(type='settlement')
        except ObjectDoesNotExist:
            transactions = Transaction.objects.none()
        return transactions
        
    def get_available_balance(self, date=None):
        """
        Get a balance of authorized and presented transaction on the date. 
        If date is None, return available balance for now.
        
        date -- datetime (django.utils.timezone) object
        """
        try:
            if date:
                transactions = self.transaction_set.filter(authorisation_date__isnull=False, authorisation_date__lte=date). \
                                                    exclude(settlement_date__isnull=False, settlement_date__lte=date)
            else:
                transactions = self.transaction_set.exclude(type='refill').exclude(type='settlement')
        except ObjectDoesNotExist:
            transactions = Transaction.objects.none()
        return transactions
        
    class Meta:
        ordering = ('card_id', 'currency',)


@receiver(pre_save, sender=Account)
def validate_account_instance(sender, **kwargs):
    account = kwargs.get('instance')
    if not account:
        return
    # do needed validations here before saving the model
    Validate(account.card_id, 'Empty card_id detected')
    Validate(account.currency, 'Empty currency detected')



class Transfer(models.Model):
    type = models.CharField(max_length=8, blank=False, choices=transfer_choices, verbose_name='Transfer type')
    complete = models.BooleanField(default=False)
    date = models.DateTimeField(default=timezone.now, verbose_name='Transfer date')
    currency = models.CharField(max_length=8, choices=currency_choices, default='EUR', verbose_name='Currency type')
    amount = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    revenue = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    
    def __str__(self):
        return '{0} {1} {2} {3} {4}/{5}'.format(self.date.strftime("%Y-%m-%d.%H:%M"), self.id, self.type, 
                                            self.currency, self.amount, self.revenue)

    def run(self):
        """
        Placeholder for the code to run actual money transfer
        """
        self.amount = self.transaction_set.aggregate(bulk=Sum('settlement_amount'))['bulk'] or 0 if self.type=='debit' \
                      else self.transaction_set.aggregate(bulk=Sum('refill_amount'))['bulk'] or 0
        self.revenue = self.transaction_set.aggregate(bulk=Sum('fee'))['bulk'] or 0
        self.save(update_fields=['amount', 'revenue'])
        # here should be the money transfer itself...
        self.date = timezone.now()
        self.complete = True
        self.save(update_fields=['date', 'complete'])



class Transaction(models.Model):
    type = models.CharField(max_length=32, blank=False, choices=transaction_choices, verbose_name='Transaction type')
    date = models.DateTimeField(default=timezone.now, verbose_name='Current Refill/Authorisation/Presentment date')
    authorisation_date = models.DateTimeField(null=True, verbose_name='Authorisation date')
    presentment_date = models.DateTimeField(null=True, verbose_name='Presentment date')
    settlement_date = models.DateTimeField(null=True, verbose_name='Settlement date')
    transaction_id = models.CharField(max_length=32, verbose_name='Transaction id')
    currency = models.CharField(max_length=8, choices=currency_choices, verbose_name='Currency type')
    billing_amount = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    settlement_amount = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    refill_amount = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    fee = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    account = models.ForeignKey('Account', on_delete=models.CASCADE, null=True)
    transfer = models.ForeignKey('Transfer', on_delete=models.CASCADE, null=True)
    ledger_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0, verbose_name='Ledger amount after transaction')
    available_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0, verbose_name='Available amount after transaction')
    transaction_data = models.CharField(max_length=4096, default='{}', verbose_name='Transaction data: json')
    
    def __str__(self):
        return '{0} {1} {2} {3} {4} {5}/{6}'.format(
                            self.date.strftime("%Y-%m-%d %H:%M"), self.account.card_id, self.transaction_id, self.type, 
                            self.currency, self.refill_amount if self.type=='refill' else self.billing_amount, self.fee)

    @staticmethod
    def create_authorisation_transaction(account, data):
        """
        Create a transaction instance of 'authorization' type.
        
        data -- verified authorisation data
        """
        def validate_input():
            # billing_amount can be 0?
            Validate(data.get('billing_amount', None) != None, 'Missing billing amount')
            Validate(data.get('type') == 'authorisation', 'Inconsistent transaction type')
        
        validate_input()
        available_amount_after_transaction = account.amount - account.get_reserved_amount() - Decimal(data.get('billing_amount', 0))
        Validate(available_amount_after_transaction >= 0, 'Not enough funds')
        now = timezone.now()
        transaction = Transaction.objects.create(
                        type=data['type'],
                        date=now,
                        authorisation_date=now,
                        transaction_id=data['transaction_id'],
                        currency=data['billing_currency'],
                        billing_amount=data['billing_amount'],
                        account=account,
                        ledger_amount=account.amount,
                        available_amount=available_amount_after_transaction,
                        transaction_data = json.dumps( {data['type']: data} )
                        )
        return transaction
        
    def do_presentment(self, data):
        """
        Update 'authorisation' transactions to 'presentment' type.
        
        data -- verified presentment data
        """
        def validate_input():
            Validate(self.type == 'authorisation', 'Inconsistent transaction type')
            Validate(data.get('type') == 'presentment', 'Inconsistent requested transaction type')
            # billing_amount can be 0?
            Validate(data.get('billing_amount', None) != None, 'Missing billing amount')

        validate_input()
        ledger_amount_after_transaction = self.account.amount - Decimal(data.get('billing_amount', 0))
        available_amount_after_transaction = ledger_amount_after_transaction - self.account.get_reserved_amount() + self.billing_amount
        Validate(ledger_amount_after_transaction >= 0, 'Not enough funds')
         
        now = timezone.now()
        self.type = data['type']
        self.date = now
        self.presentment_date = now
        self.billing_amount = Decimal(data['billing_amount'])
        self.settlement_amount = Decimal(data['settlement_amount'])
        self.fee = Decimal(data['billing_amount']) - Decimal(data['settlement_amount'])
        self.ledger_amount = ledger_amount_after_transaction
        self.available_amount = available_amount_after_transaction
        transaction_data = json.loads(self.transaction_data)
        transaction_data[data['type']] = data
        self.transaction_data = json.dumps(transaction_data)
        self.save()

