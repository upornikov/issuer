from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from app import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^api/account/$', views.AccountList.as_view(), name='account-list'),
    url(r'^api/account/(?P<pk>[0-9]+)/$', views.AccountList.as_view(), name='account-detail'),
    url(r'^api/transaction/$', views.TransactionList.as_view(), name='transaction-list'),
    url(r'^api/transaction/(?P<pk>[0-9]+)/$', views.TransactionDetail.as_view(), name='transaction-detail'),
    url(r'^api/transfer/$', views.TransferList.as_view(), name='transfer-list'),
    url(r'^api/transfer/(?P<pk>[0-9]+)/$', views.TransferDetail.as_view(), name='transfer-detail'),
    url(r'^webhook/$', views.do_transaction, name='transaction-webhook'),
    url(r'^auth/', include('rest_framework.urls', namespace='rest_framework')),]

urlpatterns = format_suffix_patterns(urlpatterns)



