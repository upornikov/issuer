from django.utils import timezone
from django.db.models import Sum
from django.test import TestCase
from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError

from decimal import Decimal
from copy import deepcopy
import time

from app.models import Account, Transaction, Transfer
from app.views import get_transactions, get_presented_transactions, get_ledger_balance, get_available_balance
from app.models import do_bulk_settlement


class InitTest(TestCase):
    def setUp(self):
        pass
        
    def tearDown(self):
        pass


    def test_account_duplicate(self):
        a1 = Account.objects.create(card_id='card1', currency='EUR')
        a11 = Account(card_id='card1', currency='FR')
        self.assertRaises(IntegrityError, a11.save)
 
 
    def test_refill_transaction(self):
        a1 = Account.objects.create(card_id='card1', currency='EUR')
        a2 = Account.objects.create(card_id='card2', currency='EUR')

        self.assertTrue(a1.amount==0)
        self.assertTrue(a2.amount==0)

        data1 = dict(card_id='card1', type='refill', refill_currency='EUR', refill_amount='100.01')
        data2 = dict(card_id='card2', type='refill', refill_currency='EUR', refill_amount='100.01')

        a1.do_refill(data1)
        self.assertEqual(a1.amount, Decimal('100.01'))
        a1.do_refill(data1)
        self.assertEqual(a1.amount, Decimal('200.02'))
        a2.do_refill(data2)
        self.assertEqual(a2.amount, Decimal('100.01'))
        a2.do_refill(data2)
        self.assertEqual(a2.amount, Decimal('200.02'))
        self.assertEqual(Transaction.objects.count(), 4)
        transactions = Transaction.objects.filter(type='refill')
        self.assertEqual(transactions.aggregate(s=Sum('fee'))['s'], Decimal('0'))
        self.assertEqual(transactions.aggregate(s=Sum('refill_amount'))['s'], Decimal('400.04'))
        self.assertEqual(Transfer.objects.count(), 4)
        transfers = Transfer.objects.filter(type='credit')
        self.assertEqual(transfers.aggregate(s=Sum('revenue'))['s'], Decimal('0'))
        self.assertEqual(transfers.aggregate(s=Sum('amount'))['s'], Decimal('400.04'))
 

 
class RefillTransactionTest(TestCase):
    def setUp(self):
        self.a = Account.objects.create(card_id='a', currency='EUR')
        # min refill data
        self.refill_data = dict(
            card_id='a', 
            type='refill',
            refill_currency='EUR', 
            refill_amount='100'
            )

    def tearDown(self):
        pass

    def test_transaction(self):
        self.a.do_refill(self.refill_data)
        # verify refill result
        self.assertTrue(self.a.amount==100, 'Wrong amount after refill')
        self.assertTrue(self.a.transaction_set.count()==1, 
            'Wrong number of transactions after refill')
        self.assertTrue(self.a.transaction_set.last().type=='refill',
            'Wrong type after refill')
        self.assertTrue(self.a.transaction_set.last().billing_amount==None,
            'Wrong billing_amount after refill')
        self.assertTrue(self.a.transaction_set.last().ledger_amount==100,
            'Wrong ledger_amount after refill')
        self.assertTrue(self.a.transaction_set.last().available_amount==100,
            'Wrong available_amount after refill')
        self.assertTrue(self.a.transaction_set.last().fee==0,
            'Wrong fee after refill')
    
    def test_all_input_faults_are_caught(self):
        """
        test_transaction is a prerequisite to make sure 
        all present_data fields are ok. 
        Now via fault injection, check any expected data field missing 
        is properly caugh.
        """
        # fault injection map to self.refill_data
        faults = [
            {'card_id': 'wrong_card_id'},
            {'type': 'wrong_type'},
            {'refill_currency': 'wrong_currency'},
            ]
        for fault in faults:
            print('expect ValidationError on:', fault)
            self.assertRaises(ValidationError, self.a.do_refill, {**self.refill_data, **fault})



class AuthorisationTransactionTest(TestCase):
    def setUp(self):
        self.a = Account.objects.create(card_id='a', currency='EUR')
        self.a.do_refill(dict(card_id='a', type='refill',
                              refill_amount='100', refill_currency='EUR'))
        # min auth data
        self.auth_data = dict(
            card_id='a', 
            type='authorisation',
            transaction_id='t1', 
            billing_currency='EUR', 
            billing_amount='10'
            )

    def tearDown(self):
        pass

    def test_transaction(self):
        self.a.do_authorisation(self.auth_data)
        # verify authorisation result
        self.assertTrue(self.a.amount==100, 'Wrong amount after authorisation')
        self.assertTrue(self.a.transaction_set.count()==2, 
            'Wrong number of transactions after authorisation')
        self.assertTrue(self.a.transaction_set.last().type=='authorisation',
            'Wrong type after authorisation')
        self.assertTrue(self.a.transaction_set.last().billing_amount==10,
            'Wrong billing_amount after authorisation')
        self.assertTrue(self.a.transaction_set.last().ledger_amount==100,
            'Wrong ledger_amount after authorisation')
        self.assertTrue(self.a.transaction_set.last().available_amount==90,
            'Wrong available_amount after authorisation')
        self.assertTrue(self.a.transaction_set.last().fee==0,
            'Wrong fee after authorisation')

    
    def test_all_input_faults_are_caught(self):
        """
        test_transaction is a prerequisite to make sure 
        all present_data fields are ok. 
        Now via fault injection, check any expected data field missing 
        is properly caugh.
        """
        # fault injection map to self.auth_data
        faults = [
            {'card_id': 'missing_card_id'},
            {'type': 'wrong_type'},
            {'transaction_id': ''},
            {'billing_currency': 'wrong_currency'},
            {'billing_amount': str(self.a.amount+1)},
            ]
        for fault in faults:
            print('expect ValidationError on:', fault)
            self.assertRaises(ValidationError, self.a.do_authorisation, {**self.auth_data, **fault})



class PresentmentTransactionTest(TestCase):
    def setUp(self):
        self.a = Account.objects.create(card_id='a', currency='EUR')
        self.a.do_refill(dict(card_id='a', type='refill',
                              refill_amount='100', refill_currency='EUR'))
        auth_data = dict(
            card_id='a', 
            type='authorisation',
            transaction_id='t1', 
            billing_currency='EUR', 
            billing_amount='10'
            )
        self.a.do_authorisation(auth_data)
        # min presentment data
        self.present_data = dict(
            card_id='a', 
            type='presentment',
            transaction_id='t1', 
            billing_currency='EUR', 
            billing_amount='11',
            settlement_currency='EUR', 
            settlement_amount='10', 
            )

    def tearDown(self):
        pass

    def test_transaction(self):
        # verify authorisation result
        self.assertTrue(self.a.amount==100, 'Wrong amount after authorisation')
        self.assertTrue(self.a.transaction_set.count()==2, 
            'Wrong number of transactions after authorisation')
        self.assertTrue(self.a.transaction_set.last().type=='authorisation',
            'Wrong type after authorisation')
        self.assertTrue(self.a.transaction_set.last().billing_amount==10,
            'Wrong billing_amount after authorisation')
        self.assertTrue(self.a.transaction_set.last().ledger_amount==100,
            'Wrong ledger_amount after authorisation')
        self.assertTrue(self.a.transaction_set.last().available_amount==90,
            'Wrong available_amount after authorisation')
        self.assertTrue(self.a.transaction_set.last().fee==0,
            'Wrong fee after authorisation')
        
        self.a.do_presentment(self.present_data)

        # verify presentment result
        self.assertTrue(self.a.amount==89, 
            'Wrong amount after presentment')
        self.assertTrue(self.a.transaction_set.count()==2, 
            'Wrong number of transactions after presentment')
        self.assertTrue(self.a.transaction_set.last().type=='presentment',
            'Wrong type after presentment')
        self.assertTrue(self.a.transaction_set.last().billing_amount==11,
            'Wrong billing_amount after presentment')
        self.assertTrue(self.a.transaction_set.last().ledger_amount==89,
            'Wrong ledger_amount after presentment')
        self.assertTrue(self.a.transaction_set.last().available_amount==89,
            'Wrong available_amount after presentment')
        self.assertTrue(self.a.transaction_set.last().fee==1,
            'Wrong fee after presentment')

    
    def test_all_input_faults_are_caught(self):
        """
        test_transaction is a prerequisite to make sure 
        all present_data fields are ok. 
        Now via fault injection, check any expected data field missing 
        is properly caugh.
        """
        # fault injection map to self.present_data
        faults = [
            {'card_id': 'wrong_card_id'},
            {'type': 'wrong_type'},
            {'transaction_id': ''},
            {'billing_currency': 'wrong_currency'},
            {'billing_amount': str(self.a.amount+1)},
            {'settlement_currency': 'wrong_currency'},
            {'settlement_amount': str(self.a.amount+1)},
            ]
        for fault in faults:
            print('expect ValidationError on:', fault)
            self.assertRaises(ValidationError, self.a.do_presentment, {**self.present_data, **fault})



class SettlementTransactionTest(TestCase):
    def setUp(self):
        self.total_amount = Decimal('400.00')
        self.a = Account.objects.create(card_id='a', currency='EUR')
        self.b = Account.objects.create(card_id='b', currency='EUR')
        self.c = Account.objects.create(card_id='c', currency='EUR')
        self.d = Account.objects.create(card_id='d', currency='EUR')
        refill_data = dict(
            card_id='x', 
            type='refill',
            refill_amount='100', 
            refill_currency='EUR',
            )
        self.a.do_refill({**refill_data, **dict(card_id='a')})
        self.b.do_refill({**refill_data, **dict(card_id='b')})
        self.c.do_refill({**refill_data, **dict(card_id='c')})
        self.d.do_refill({**refill_data, **dict(card_id='d')})

        auth_data = dict(
            card_id='x', 
            type='authorisation',
            transaction_id='t1', 
            billing_currency='EUR', 
            billing_amount='10'
            )
        self.a.do_authorisation({**auth_data, **dict(card_id='a')})
        self.b.do_authorisation({**auth_data, **dict(card_id='b')})
        self.c.do_authorisation({**auth_data, **dict(card_id='c')})
         
        self.present_data = dict(
            card_id='x', 
            type='presentment',
            transaction_id='t1', 
            billing_currency='EUR', 
            billing_amount='11',
            settlement_currency='EUR', 
            settlement_amount='10', 
            )
        self.a.do_presentment({**self.present_data, **dict(card_id='a')})
        self.b.do_presentment({**self.present_data, **dict(card_id='b')})

    def tearDown(self):
        pass

    def test_transaction_maths(self):
        do_bulk_settlement()
        
        self.assertTrue(Transfer.objects.count()==5,
            'Wrong number of tranfers after settlement')
        self.assertTrue(Transaction.objects.count()==7,
            'Wrong number of transactions after settlement')
        self.assertTrue(Transaction.objects.filter(type='refill').count()==4,
            'Wrong number of auth transactions after settlement')
        self.assertTrue(Transaction.objects.filter(type='authorisation').count()==1,
            'Wrong number of auth transactions after settlement')
        self.assertTrue(Transaction.objects.filter(type='presentment').count()==0,
            'Wrong number of presentment transactions after settlement')
        self.assertTrue(Transaction.objects.filter(type='settlement').count()==2,
            'Wrong number of settlement transactions after settlement')
        
        unpaid_debt = \
            Transaction.objects.filter(type='authorisation').aggregate(s=Sum('billing_amount'))['s']
        self.assertTrue(unpaid_debt==10, 'Wrong unpaid_debt after settlement')

        transfer_amount = Transfer.objects.all().last().amount
        transfer_revenue = Transfer.objects.all().last().revenue
        self.assertTrue(transfer_amount==20,
            'Wrong transfer amount after settlement %s'%transfer_amount)
        self.assertTrue(transfer_revenue==2,
            'Wrong transfer revenue after settlement')
        
        total_credit = \
            Transaction.objects.filter(type='refill').aggregate(s=Sum('refill_amount'))['s'] 
        self.assertTrue(total_credit==self.total_amount,
            'Wrong transfer revenue after settlement')
        
        paid_debt = \
            Transaction.objects.filter(type='settlement').aggregate(s=Sum('settlement_amount'))['s'] 
        paid_fee = \
            Transaction.objects.filter(type='settlement').aggregate(s=Sum('fee'))['s'] 
        self.assertTrue(paid_debt==transfer_amount, 'Wrong paid debt after settlement')
        self.assertTrue(paid_fee==transfer_revenue, 'Wrong paid fee after settlement')
            
        total_card_amount = Account.objects.all().aggregate(s=Sum('amount'))['s']
        self.assertTrue(total_card_amount+paid_debt+paid_fee == total_credit,
            'Wrong total balance after settlement')


class BalanceApiTest(TestCase):
    def setUp(self):
        self.tests = []
        
    def tearDown(self):
        pass 
        
    def create_test_1(self, func, card_id, start_date, end_date, expected_result):
        res = dict(
            func=func,
            kwargs=dict(
                card_id=card_id,
                start_date=start_date, 
                end_date=end_date,
                ),
            expected_result=expected_result
        )
        self.tests.append(res)
        
    def create_test_2(self, func, card_id, date, expected_result):
        res = dict(
            func=func,
            kwargs=dict(
                card_id=card_id,
                date=date, 
                ),
            expected_result=expected_result
        )
        self.tests.append(res)
        
    def test_balance_after_transaction_cycle(self):
        # create refill-auth-present-settle transactions cycle
        # with timestamps to check against
        # for the sake of time (case init takes longest), put it into a single func
        start_date = timezone.now()
        time.sleep(0.5)
        account = Account.objects.create(card_id='card_id_2', currency='EUR')
        refill_data = dict(
            card_id='card_id_2', 
            type='refill',
            refill_amount='100', 
            refill_currency='EUR',
            )
        account.do_refill({**refill_data, **{'transaction_id':'r1'}})
        account.do_refill({**refill_data, **{'transaction_id':'r2'}})
        time.sleep(0.2)
        refill_done_date = timezone.now()
        time.sleep(0.2)

        auth_data = dict(
            card_id='card_id_2', 
            type='authorisation',
            transaction_id='X', 
            billing_currency='EUR', 
            billing_amount='10'
            )
        account.do_authorisation({**auth_data, **{'transaction_id': 't1'}})
        account.do_authorisation({**auth_data, **{'transaction_id': 't2'}})
        account.do_authorisation({**auth_data, **{'transaction_id': 't3'}})
        account.do_authorisation({**auth_data, **{'transaction_id': 't4'}})
        time.sleep(0.2)
        authorisation_done_date = timezone.now()
        time.sleep(0.2)

        present_data = dict(
            card_id='card_id_2', 
            type='presentment',
            transaction_id='X', 
            billing_currency='EUR', 
            billing_amount='11',
            settlement_currency='EUR', 
            settlement_amount='10', 
            )
        account.do_presentment({**present_data, **{'transaction_id': 't1'}})
        account.do_presentment({**present_data, **{'transaction_id': 't2'}})
        time.sleep(0.2)
        half_presentment_done_date = timezone.now()
        time.sleep(0.2)

        do_bulk_settlement()
        account.do_presentment({**present_data, **{'transaction_id': 't3'}})
        time.sleep(0.2)
        presentment_done_date = timezone.now()

        # collect test metrix and expected results
        self.create_test_1(get_transactions, 'card_id_2', start_date, presentment_done_date, ['r1', 'r2', 't1', 't2', 't3'])
        self.create_test_1(get_transactions, 'card_id_2', None, half_presentment_done_date, ['r1', 'r2', 't1', 't2',])
        self.create_test_1(get_transactions, 'card_id_2', authorisation_done_date, presentment_done_date, ['t1', 't2', 't3'])
        self.create_test_1(get_transactions, 'card_id_2', authorisation_done_date, None, ['t1', 't2', 't3'])
        self.create_test_1(get_transactions, 'card_id_2', None, None, ['r1', 'r2', 't1', 't2', 't3'])
        
        self.create_test_1(get_presented_transactions, 'card_id_2', start_date, presentment_done_date, ['t1', 't2', 't3'])
        self.create_test_1(get_presented_transactions, 'card_id_2', None, half_presentment_done_date, ['t1', 't2'])
        self.create_test_1(get_presented_transactions, 'card_id_2', half_presentment_done_date, presentment_done_date, ['t3'])
        self.create_test_1(get_presented_transactions, 'card_id_2', authorisation_done_date, None, ['t1', 't2', 't3'])
        self.create_test_1(get_presented_transactions, 'card_id_2', None, None, ['t1', 't2', 't3'])

        self.create_test_2(get_ledger_balance, 'card_id_2', authorisation_done_date, [])
        self.create_test_2(get_ledger_balance, 'card_id_2', half_presentment_done_date, ['t1', 't2'])
        self.create_test_2(get_ledger_balance, 'card_id_2', presentment_done_date, ['t3'])
        self.create_test_2(get_ledger_balance, 'card_id_2', None, ['t3'])

        self.create_test_2(get_available_balance, 'card_id_2', authorisation_done_date, ['t1', 't2', 't3', 't4',])
        self.create_test_2(get_available_balance, 'card_id_2', half_presentment_done_date, ['t1', 't2', 't3', 't4',])
        self.create_test_2(get_available_balance, 'card_id_2', presentment_done_date, ['t3', 't4',])
        self.create_test_2(get_available_balance, 'card_id_2', None, ['t3', 't4',])

        # run the tests
        for test in self.tests:
            qs = test['func'](**test['kwargs'])
            res = sorted(list(qs.values_list('transaction_id', flat=True)))
            self.assertTrue(res==test['expected_result'],
                           'Failed expected %s : %s)'%(test, res))

