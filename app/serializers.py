from rest_framework import serializers
from app.models import Account, Transaction, Transfer


class AccountSerializer(serializers.HyperlinkedModelSerializer):
    transaction_set = serializers.HyperlinkedRelatedField(many=True, view_name='transaction-detail', read_only=True)
    class Meta:
        model = Account
        fields = ('url', 'card_id', 'currency', 'amount', 'transaction_set')


class TransactionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Transaction
        fields = ('url', 'type', 'date', 'transaction_id', 
                  'authorisation_date', 'presentment_date', 'settlement_date', 
                  'currency', 'billing_amount', 'settlement_amount', 'refill_amount', 
                  'fee', 'account', 'transfer', 'ledger_amount', 'available_amount', 
                  'transaction_data')

                  
class TransferSerializer(serializers.HyperlinkedModelSerializer):
    transaction_set = serializers.HyperlinkedRelatedField(many=True, view_name='transaction-detail', read_only=True)
    class Meta:
        model = Transfer
        fields = ('url', 'type', 'date', 'currency', 'amount', 'revenue', 'complete', 'transaction_set')

    