from django.shortcuts import render
from django.http import HttpResponse

from app.models import Account, Transaction, Transfer
from app.models import transaction_fields
from app.forms import TransactionForm
from app.serializers import AccountSerializer, TransactionSerializer, TransferSerializer

from rest_framework import generics
#from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import status


@api_view(['GET'])
def index(request, format=None):
    return Response({
        'accounts': reverse('account-list', request=request, format=format),
        'transfers': reverse('transfer-list', request=request, format=format),
        'transactions': reverse('transaction-list', request=request, format=format),
        'testpage': reverse('transaction-webhook', request=request, format=format)
    })


class AccountList(generics.ListCreateAPIView):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
#    permission_classes = (permissions.IsAuthenticated,)

class AccountDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
#    permission_classes = (permissions.IsAuthenticated,)


class TransferList(generics.ListAPIView):
    queryset = Transfer.objects.all()
    serializer_class = TransferSerializer
#    permission_classes = (permissions.IsAuthenticated,)

class TransferDetail(generics.RetrieveAPIView):
    queryset = Transfer.objects.all()
    serializer_class = TransferSerializer
#    permission_classes = (permissions.IsAuthenticated,)


class TransactionList(generics.ListAPIView):
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
#    permission_classes = (permissions.IsAuthenticated,)

class TransactionDetail(generics.RetrieveAPIView):
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
#    permission_classes = (permissions.IsAuthenticated,)



def do_transaction(request):
    """
    Schema transaction webhook endpoint.
    
    request - Schema transaction request dict - see models.transaction_fields
    """
    if request.method == 'POST':
        data = dict()
        # since 'data' may be audited, get only transation fields 
        for f in transaction_fields:
            if f in request.POST:
                data[f] = request.POST[f]
        try:
            account = Account.objects.get(card_id=data['card_id'])
            if data['type'] == 'authorisation':
                account.do_authorisation(data)
            elif data['type'] == 'presentment':
                account.do_presentment(data) 
            else:
                raise Exception("Invalid transaction type")
        except Exception as e:
            print('do_transaction:', str(e))
            return HttpResponse(status.HTTP_403_FORBIDDEN)
        return HttpResponse(status.HTTP_200_OK)
    else:
		# this part is for testing
        form = TransactionForm()
    return render(request, 'app/testapp.html', 
                            {'transaction_form': form, 
                             'accounts': Account.objects.all(),
                             'transfers': Transfer.objects.all()
                            })


def get_transactions(card_id, start_date=None, end_date=None):
    """
    Get all transactions, excluding authorisation type, for a specific timeframe. 
    If no dates, return all the transactions from the card.
    If start_date is None, return transactions before the end_date.
    If end_date is None, return transactions since the start_date.
    
    card_id - card ID
    start_date, end_date -- datetime(django.utils.timezone) objects for the timeframe
    """
    account = Account.objects.get(card_id=card_id)
    return account.get_transactions(start_date, end_date)

    
def get_presented_transactions(card_id, start_date=None, end_date=None):
    """
    Get presented transactions for a specific timeframe (including settled ones). 
    If no dates, return all the transactions from the card.
    If start_date is None, return transactions before the end_date.
    If end_date is None, return transactions since the start_date.
    
    card_id - card ID
    start_date, end_date -- datetime(django.utils.timezone) objects for the timeframe
    """
    account = Account.objects.get(card_id=card_id)
    return account.get_presented_transactions(start_date, end_date)

    
def get_ledger_amount(card_id, date=None):
    """
    Get ledger amount on the card for the date. 
    If date is None, return current ledger amount.
    
    date -- datetime (django.utils.timezone) object
    """
    account = Account.objects.get(card_id=card_id)
    return account.get_ledger_amount(date)
    

def get_available_amount(card_id, date=None):
    """
    Get available amount on the card for the date (authorised 
    reservations accounted for). If date is None, return available amount.
    
    date -- datetime (django.utils.timezone) object
    """
    account = Account.objects.get(card_id=card_id)
    return account.get_available_amount(date)


def get_ledger_balance(card_id, date=None):
    """
    Get a balance of presented transactions on the date. 
    If date is None, return current ledger balance.
    
    date -- datetime (django.utils.timezone) object
    """
    account = Account.objects.get(card_id=card_id)
    return account.get_ledger_balance(date)
    

def get_available_balance(card_id, date=None):
    """
    Get a balance of authorized and presented transaction on the date. 
    If date is None, return available balance for now.
    
    date -- datetime (django.utils.timezone) object
    """
    account = Account.objects.get(card_id=card_id)
    return account.get_available_balance(date)
