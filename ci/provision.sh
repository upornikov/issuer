#!/bin/sh

sudo apt-get update -y
sudo apt-get install -y curl git

sudo apt-get install -y software-properties-common
sudo curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
sudo apt-get install apt-transport-https
sudo apt-get update -y
sudo apt-get install -y docker-ce

sudo curl -L https://github.com/docker/compose/releases/download/1.19.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod 755 /usr/local/bin/docker-compose

#install docker-machine for autoscaling with docker-machine


sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-ci-multi-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-ci-multi-runner-linux-amd64 && \
sudo chmod 755 /usr/local/bin/gitlab-runner

# Create new user for runner
# !!!sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --password Password12 --shell /bin/bash
 
# unittest runner: tests are run in dynamically created individual containers
sudo gitlab-runner unregister --name "Issuer scalable ut runner" >nul 2>&1
sudo gitlab-runner register \
    	-u https://gitlab.com \
    	-r Tsz5EwM_d6YSNVcwLdYm \
    	-n \
    	--executor docker \
    	--docker-image python \
    	--tag-list "python,ut" \
    	--name "Issuer scalable ut runner"
 
# staging deployment runner: persistent container for Issuer staging deployment
sudo gitlab-runner unregister --name "Issuer deploy runner" >nul 2>&1
sudo gitlab-runner register \
    	-u https://gitlab.com \
    	-r Tsz5EwM_d6YSNVcwLdYm \
    	-n \
    	--executor shell \
    	--tag-list "python,deploy" \
    	--name "Issuer deploy runner"

# !!!sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner install --user=vagrant --working-directory=/home/vagrant

sudo sed -i -e "s/concurrent = 1/concurrent = 3/" /etc/gitlab-runner/config.toml
sudo gitlab-runner start

