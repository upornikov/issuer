# Notes:

- Account currency type is fixed - other currencies are not accepted;
- Schema is aware of the account currency type and Schema itself provides currency conversion in transaction request.
- billing_currency/settlement_currency currency conversion is not in scope (otherwise rates should be provided by schema).
- Schema is responsible for providing interchange_revenue=billing_amount-settlement_amount.
- Transaction model is redundant (for convenience) - can be made more concise, if needed.
- Transactions also include 'refill' type for updating account balance.
- On presentment, account debit is adjusted and is consistent: it holds actual amount on the account. 
- On presentment, the authorization transaction is adjusted: now the transaction (new type: presentment) holds final debt to Schema.
- Desing limitation: billing_currency==settlement_currency, since debt is paid from the account with fixed currency type.
- On settlement, there goes simulated money transfer by presentment transaction, and presentment type is changed to settlement.
- the balance internal APIs provide convenience functions for returning transactions in various combinations: see corresponging function descriptions.


# Project location

Project is available in https://gitlab.com/upornikov/issuer

# Deliveries:

- webhook ext. API (ext API for executing transactions): 
    POST /webhook/
    
- simple test app:
    GET /webhook/

- models: 
    - app.models.Account, 
    - app.models.Transaction, 
    - app.models.Transfer

- REST API (/webhook post is the only endpoint for executing transactions): 
    - /api/account/  (GET, POST, HEAD, PUT, PATCH, DELETE)
    - /api/transaction/ (GET) 
    - /api/transfer/ (GET)

- balance internal API
    - app.views.get_presented_transactions
    - app.views.get_transactions
    - app.views.get_ledger_balance
    - app.views.get_available_balance 
    - app.views.get_ledger_amount
    - app.views.get_available_amount

- management commands:
    - load_money
    - do_settlement

- CI/CD
    - Gitlab.com manages the project CI/CD functions via local VirtualBox instance:
        - CI/CD features:
            - automated unittests;
            - manually triggered debug deployment on django dev.server with debugging capabilities;
            - manually triggered staging deployment of debug version with nginx and gunicorn as app.server;
        - vagrant is used as hypervisor;
        - create local VirtualBox instance by running:
            > cd ci
            > vagrant up
        - on creation, the instance registers 2 runners: unittest runner and deployment runner;
        - unittests are run on dynamically created/destroyed docker containers;
        - deployments are done on dedicated docker containers, created by CD automation script;
        - the CI/CD functions are manipulated via gitlab.com project interface;
    - unittest automation is run on each commit;
    - unittests can be run on parallel containers;
    - if unittests are a success, manual debug and staging deployments come available;
    - the VirualBox instance does portforwarding from docker containers to the host:
        - debug deployment is available on localhost:8082
        - staging deployment is available on localhost:8083
    
